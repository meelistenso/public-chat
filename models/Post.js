var mongoose = require('mongoose');
var User = mongoose.model('User');

var PostSchema = new mongoose.Schema({
  text: { type: String, trim: true, required: [true, "is invalid"], maxlength: 200 },
  author: { type: mongoose.Schema.Types.ObjectId, ref: 'User' }
}, {timestamps: true});

PostSchema.methods.toJSONFor = function(user){
  return {
    createdAt: this.createdAt,
    updatedAt: this.updatedAt,
    text: this.text,
    author: this.author.toProfileJSONFor(user)
  };
};

mongoose.model('Post', PostSchema);
