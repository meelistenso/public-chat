var router = require('express').Router();
var mongoose = require('mongoose');
var Post = mongoose.model('Post');
var User = mongoose.model('User');
var auth = require('../auth');

router.get('/', auth.optional, function(req, res, next) {
  var query = {};
  var limit = 20;
  var offset = 0;

  if(typeof req.query.limit !== 'undefined'){
    limit = req.query.limit;
  }

  if(typeof req.query.offset !== 'undefined'){
    offset = req.query.offset;
  }

  Promise.all([
    req.query.author ? User.findOne({username: req.query.author}) : null
  ]).then(function(results){
    var author = results[0];

    if(author){
      query.author = author._id;
    }

    return Promise.all([
      Post.find(query)
        .limit(Number(limit))
        .skip(Number(offset))
        .sort({createdAt: 'desc'})
        .populate('author')
        .exec(),
        Post.count(query).exec(),
      req.payload ? User.findById(req.payload.id) : null,
    ]).then(function(results){
      var posts = results[0];
      var postsCount = results[1];
      var user = results[2];

      return res.json({
        posts: posts.map(function(post){
          return post.toJSONFor(user);
        }),
        postsCount: postsCount
      });
    });
  }).catch(next);
});

router.post('/', auth.required, function(req, res, next) {
  User.findById(req.payload.id).then(function(user){
    if (!user) { return res.sendStatus(401); }

    var post = new Post(req.body.post);

    post.author = user;

    return post.save().then(function(){
      return res.json({post: post.toJSONFor(user)});
    });
  }).catch(next);
});

module.exports = router;
